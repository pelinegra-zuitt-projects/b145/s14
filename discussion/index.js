
console.log("Hello from JS");

console.log("example statement");

// syntax:console(value/message);

//[DECLARING VARIABLES]

// -> tell our devices that a variable name is created and ready to store data.

// syntax: let/const desiredVariableName;
let myVariable;

//What is a variable
// It is used to contain/store data

// What are the benefits of utilizing a variable? 
// This makes it easier for us to associate information stored in our devices to actual names about the information

// an assignment operator  (=) is used to assign or pass down values into a variable
let clientName = "Juan Dela Cruz";
let contactNumber = "09951446335";

// [PEEKING INSIDE A VARIABLE]
let greetings; 

console.log(clientName)
console.log(contactNumber)
console.log(greetings)
let pangalan = "John Doe"
console.log(pangalan)

//If we would try to print out a value of a variable that has not been declared, it will return an error of "undefined."
// Variable exists but no assigned value so undefined state

// DATA TYPES
// 3 Forms of Data types
// 1. Primitive -> string, numbers and boolean; contains only a single value:
// 2. Composite -> can contain multiple values; array and objects
// 3. Special --> null and undefined

// 1. String - comb alphanumeric characters
let country = "Philippines";
let province = 'Metro Manila';
// 2. Number - include positive negative numbers with decimals
let headcount = 26; 
let grade = 98.7;

// 3. Boolean contains two logical values; true or false
let isMarried = false;
let inGoodConduct = true;

// 4. Null indicates the absence of a value
let spouse = null
console.log(spouse);

// 5. Undefined - represents a state of variable that has been declared but without an assigned value

// 6. Arrays are a special knd of composite data type that's used to store multiple values. Arrays can store different data types but is normally used to 
// [] --> array container
// Let's create a collection of all subjects in the bootcamp
let bootcampSubjects = ["HTML","CSS", "Bootstrap", "Javascript"];
// display the output of the array inside the console
console.log(bootcampSubjects);
// Rule fo thumb when declaring array structures 
// Storing multiple data types in an array is NOT recommended

// An array should be a collection of data that describes a similar/single topic or subject
let details = ["Keanu", "Reeves", 32, true];
console.log(details);
// Objects
// Objects are another special kind of composite data type that is used to mimic or represent a real world object/item
// Anatomy of JS object is different with the anatomy of JS array
// Objects are used to create complex data that contains pieces of information that are relevant to each other
// Every individual pieve of code/info is called property of an object


// SYNTAX: 
// let/const objectName = {
// 	key -> value
// 	propertyA: value,
// 	propertyB: value
// }


let cellphone = {
	brand: 'Samsung',
	model: 'A12',
	color: 'Black',
	serialNo: 'AX12002122',
	isHomeCredit: true,
	features: ["Calling","Texting","Ringing","5G"],
	price: 8000
}

// let's display the object inside the console
console.log(cellphone);



// 7. Object contains other data like a persos parts of a full name or a set of information array of personal information for diff persons

// null --> placeholder
// undefined --> state of failure to declare the variable


// Variables and Constants

// Variables - are used to store or contain data
// the values/info stored inside the variable can be changed or repackaged
let personName = "Michael";
console.log(personName)

// If you are going to reassign a new value for a variable, you no longer have to use the "let" again
personName = "Jordan"
console.log(personName);

// learn how to concatenate strings (+)

// => join, combine, link
let pet = "dog";
console.log("this is the initial value of the var: " + pet);
pet = "cat"
console.log("This is the new value of the var: " + pet);

// Constants
// permanent, fixed, absolute
// the value assigned on a constant cannot be changed
//syntax: const desiredName = value;
const PI = 3.14;
console.log(PI)

const year = 12;
console.log(year);

const familyName = 'Aclan'
console.log(familyName)

// Let's try to reassign a new value in a constant
let familyName = 'Castillo'
console.log(familyName) ==> error

// constant's initial value is permanent; you cannot re-declare a constant twice
